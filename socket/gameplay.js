import { texts, greetings, transport, jokes } from '../data'

export default io => {
    io.on("connection", socket => {

        const username = socket.handshake.query.username;

        socket.emit('selectTypingText', texts[Math.floor(Math.random() * texts.length)])

        socket.emit('startGreeting', {
            greetings: getRandomArrayElement(greetings),
            username,
            transport: getRandomArrayElement(transport)
        })

        socket.on('needJoke', () => {
            socket.emit('takeJoke', {
                joke: getRandomArrayElement(jokes)
            })
        })


    });

    function getRandomArrayElement(arr) {
        return arr[Math.floor(Math.random() * arr.length)]
    }
};
import * as config from "./config";

import gameSocket from './game'
import gamePlaySocket from './gameplay'

export default io => {
  gameSocket(io.of('/game'))
  gamePlaySocket(io.of('/gameplay'))
}
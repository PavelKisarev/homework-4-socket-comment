const rooms = new Map()
const activeUsers = new Map()

export default io => {
    io.on("connection", socket => {

        const username = socket.handshake.query.username;

        if (!activeUsers.has(username)) {
            activeUsers.set(username, socket.id)
        }
        else {
            socket.emit('UserAlreadyExist', username)
        }

        socket.on('CreateRoom', (roomId) => {
            socket.join(roomId, () => {
                io.to(socket.id).emit("JoinRoomDone", roomId);
            })
        })

        socket.on('disconnect', () => {
            activeUsers.delete(username)
        })

    });
};
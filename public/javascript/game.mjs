const username = sessionStorage.getItem("username");


if (!username) {
  window.location.replace("/login");
}

const socket = io("", { query: { username } });

socket.on('UserAlreadyExist', username => {
  sessionStorage.clear();
  window.location.replace('/login')
  alert(`User ${username} already exist`);
})

const addRoomBtn = document.getElementById('add-room-btn')
addRoomBtn.addEventListener('click', () => {
  let roomId = prompt('Enter room name');
  socket.emit('CreateRoom', roomId);
})

const roomsContainer = document.getElementById('rooms-page')
const gameContainer = document.getElementById('game-page')
socket.on('JoinRoomDone', roomId => {
  roomsContainer.classList.add('display-none');
  gameContainer.classList.remove('display-none')
})


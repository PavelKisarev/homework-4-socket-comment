const username = sessionStorage.getItem("username")
const socket = io("http://localhost:3002/gameplay", { query: { username } })

const target = document.getElementById('keyb')
const progress = document.getElementById('bar')
const popup = document.getElementById('popup')
const popupCloseBtn = document.getElementById('close-popup')
const commentBlock = document.getElementById('comments')





let index = 0
let curProgress = 0
document.addEventListener('keydown', checkGameProcess)

popupCloseBtn.addEventListener('click', () => {
    popup.classList.remove('show')
})

let str, strLength, rng, sel

socket.on('selectTypingText', (text) => {
    target.innerHTML = text
    str = target.innerText
    strLength = str.length

    if (document.createRange) {
        rng = document.createRange();
        rng.setStart(target, 0);
        sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(rng);
    } else {
        rng = document.body.createTextRange();
        rng.moveToElementText(target);
        rng.select();
    }
})

socket.on('startGreeting', () => {
    addCommentElem('Вас приветствует комментатор!!! Сегодня ожидается очень напряженная гонка')
})
socket.on('startGreeting', greeting)

socket.on('takeJoke', ({ joke }) => {
    addCommentElem(joke)
})

let intervalID = setInterval(() => {
    currentRaceStatus(curProgress)
}, 30000);

let intervalID2 = setInterval(() => {
    socket.emit('needJoke')
}, 2000);


function checkGameProcess(e) {
    console.log('' + str.charAt(index) + ' === ' + e.key)
    console.log('indx = ', index)
    console.log('strLength = ', strLength)
    if (str.charAt(index) === e.key) {
        sel.modify('extend', 'right', 'character')
        index++
    }
    if (index === strLength) {
        finishGame()
    }
    curProgress = Math.floor((index / strLength) * 100)
    progress.style.width = `${curProgress}%`
    console.log('progress = ', curProgress)

    if (index === strLength - 5) {
        addCommentElem(`${username} на финишной прямой. Осталось еще немного!`)
    }

}

function currentRaceStatus(progress) {
    let str = `${username} неуклонно приближается к финишу. ${progress}% трассы уже пройдено`
    addCommentElem(str)
}


function greeting({ greetings, username, transport }) {
    let curTextCommnet = `${greetings}, ${username}. Cегодня на ${transport} будет покорять гоночную трассу`
    addCommentElem(curTextCommnet)
}

function addCommentElem(str) {
    const li = document.createElement('li')
    li.innerHTML = str
    commentBlock.append(li)
    commentBlock.scrollTo(0, commentBlock.scrollHeight);
}


function finishGame() {
    progress.style.backgroundColor = 'green'
    let timeId = setTimeout(() => {
        addCommentElem(`${username} завершает гонку.`)
        clearInterval(intervalID)
        clearInterval(intervalID2)
        document.removeEventListener('keydown', checkGameProcess)
    }, 1000)

}



